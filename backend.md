### *Mobile/back-end/programming*

## History Repeating Itself



[A blockchain-based Redux clone in 105 lines of code](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-b/) [swizec]. See, I would&#8217;ve gone for 103, but sure!

And, hey, another great new SitePoint book: [back-end performance](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-n/) [sitepoint].



## Jailhouse Rock



[Best-practices for Node.js development](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-o/) [nemethgergley].

[How to do 
common JavaScript tasks without using Redux](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-b/) [medium/@tarkus].

[Supermax](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-n/) [supermax] is an API making it easier to manage Ethereum data.

 
 

## Rhyme or Reason



[An intro to using ReasonML, Facebook&#8217;s new programming language for writing React apps with OCaml syntax](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-u/) [medium/oke-software-poland].

[Bucket Stream](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-o/) [github/eth0izzle] is a tool for checking out interesting Amazon S3 buckets by watching certificate transparency logs.

 

## Everything is Wrapped Up in a Nice Little Package!*



[How to use CrateDB and PopSQL for collaborative SQL editing](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-o/) [crate].

[Walt](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-b/) [github/ballercat] is a JavaScript-like syntax for WebAssembly text format.

Web app bundler Parcel has been coming up a bit lately, [here&#8217;s everything you need to know about it](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-n/) [medium/@wesharehoodies].

[Docusaurus](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-p/) [github/facebook] is a project from Facebook offering easy-to-maintain open source docs websites.

* [Credit: Homer Simpson](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-x/) [youtube/mostlysimpsons]


## Wake Up Call



[An intro to the joys of using CouchDB, PouchDB and Hoodie as a stack for PWAs](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-u/) [medium/offline-camp].

[Insomia](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-o/) [insomnia.rest] is a REST client that seems pretty lovable.

[Joy](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-b/) [github/matthewmueller] is a Go to JavaScript compiler.

[Microbundle](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-n/) [github/developit] is a zero-configuration bundler for tiny modules, it&#8217;ll bundle your library with nothing but a package.json.



## Easy as A, B, C,+,+



Suggestion: [speed up your Node app with native C++ addons](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-k/) [medium/the-node-js-collection].

[Vaxic](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-u/) [github/ethanent] is an ultra-light, Node web framework.

Idea: [learned index structures for data management systems](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-o/) [arxiv].

[Expo](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-b/) [expo] is a toolchain built around React Native to help you build native iOS and Android apps using JavaScript, and React.

[Truffle Suite](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-n/) [truffleframework] is an Ethereum Swiss Army Knife.



## Wake Up and Smell the Coffee



[*Performance Project*](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-h/) [sitepoint] is a new book from us, a thorough look at building and improving the performance of an image gallery blog.

[Macchiato](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-k/) [macchiato-framework] is a way to use ClojureScript to build Node apps.

[Awesome Data Science](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-u/) [github/bulutyazilim] is a collection of data science resources, helping you become awesome at the subject.

[&#8220;Mobile first&#8221; might already be a bit outdated](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-o/) [blog.intercom].

 


## Sappy-Go-Lucky



[An intro to RubyMine, a useful IDE for Ruby and Rails devs](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-k/) [sitepoint].

[A guide to setting up webpack for any project](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-u/) [scotch].

[Questions you should ask yourself when writing tests](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-o/) [charemza].

[An introduction to the Web Payment API](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-b/) [smashingmagazine].

[An intro to Sapper, a Node.js framework, inspired by Next.js, that offers lightweight progressive web app creation](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-n/)[svelte.technology].

[wav2letter](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-p/) [github/facebookresearch] is an end-to-end automatic speech recognition system from Facebook.

[A curated list of development checklists](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-x/) [github/huyingjie]. Which I&#8217;ve now included in a curated newsletter&#8230; 
this is getting deep... 

 


## Parse the Salt



[How to become a better Node developer in 2018](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-u/) [nemethgergely].

[A 
list of fun apps to build](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-o/) [medium.freecodecamp].

[RSParser](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-b/) [github/brentsimmons] is a nice bit of Objective-C/Swift/HTML code for parsing RSS, Atom, JSON, OPML and HTML.

[You can now just program on a quantum computer, right now, for free](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-n/) [quantumexperience]. I guess that is a bit futuristic, which is timely given we&#8217;re now living in 2018.

[A counterpoint to the idea you should never let a user, when failing to login, know whether the username or password was incorrect, as that&#8217;ll give the game away to a hacker](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-p/) [hackernoon]. The author is a bit sweary, but does have a good point!

 
## Stacks on Stacks



[How to secure your web app with HTTP headers](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-k/) [smashingmagazine].

[The stacks of Facebook, Netflix and other tech giants](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-u/) [blog.keen].

[LocalStack](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-o/) [github/localstack] is a nice little local AWS stack for developing your little pretties offline.

[The JavaScript Performance API, broken down](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-b/) [css-tricks], but in a good way.

And [an intro to the Web Share API](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-n/) [codeburst].



## You Got Served



[fac](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-d/) [github/mkchoi212] is a little command line interface for solving git conflicts.

[Majestic](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-h/) [github/raathigesh] is a nice UI for running tests in Jest.

[Pragma](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-k/) [withpragma] aims to be a home base for Ethereum developers.

[Uppy](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-u/) [github/transloadit] is an open source file uploader for web browsers.

[A serverless starter kit](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-o/) [trackchanges].

 
 


 
