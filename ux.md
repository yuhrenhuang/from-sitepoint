### *Design/UX/product*

## I Was Just Fontin&#8217;



[7 weird fonts from 2017](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-c/) [blog.prototypr].

This designer built and shipped an iOS app in six months [here&#8217;s how](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-q/) [medium/sofa-blog].

[How to use Figma to build a component library/style guide](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-a/) [blog.prototypr].

 

## On Spec



[Tracy converts sketches to SVG](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-x/) [usetracy].

[The best design books that aren&#8217;t 
about design](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-m/) [kottke].

[Spectral](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-c/) [design.google] is a new screen-first typeface from Google.

[How to ship and validate new projects fast](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-q/) [sitepoint].

 

 

## Fine-Tune



[A nice piece on managing to continue learning on a team in which you&#8217;re the only designer/UXer](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-c/) [medium/the-year-of-the-looking-glass].

[How to think about a UX revamp in an enterprise app](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-q/) [sitepoint].

A little bit of # inspo for you: [NinjaTune&#8217;s 2017 review](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-a/) [ninjatune]. Also, the music is very good.

 


## Solving for X



[How to design for iPhone X without an iPhone X](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-c/) [blog.halide].

[Why Twitter built 
a design system](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-q/) [medium/twitter-design-research].

[Sketch v Figma v Affinity - a review of the most popular UI design tools of 2017](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-a/) [designrevision].

[When designing for customization - don&#8217;t forget about the defaults!](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-f/) [trackchanges.postlight].



## Future Shock



[An &#8220;interactive article&#8221; featuring either plausible or absurd predictions for the UX industry in 2018](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-a/) [uxdesign].

And [a non-interactive article where 18 designers predict UI and UX trends for 2018](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-f/) [blog.figma].

A look at [the value of design thinking in business](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-z/) [toptal].

 

## Heart Skips a Beat



[How skip-ink underlines improve text readability for dyslexic users](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-q/) [medium/@iamhiwelo].

[5 A/B testing tools for making data-driven design decisions](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-a/) [sitepoint].

[Advice for a junior designer](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-f/) [blog.prototypr].

 

## Call, Raise or Fold



Facebook released [Origami, Sketch and Photoshop templates of iOS 11 GUI elements for you to use for your designs](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-c/) [facebook.design].

Speaking of, [Elements](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-q/) [sketchapp] is a free iOS UI kit for Sketch, provided by&#8230;. Sketch.

[These logos are blurry, and your job is to identify them](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-a/) [jim-nielsen]. I&#8217;m pretty good at this, because I&#8217;m very short-sighted and this the default view I have.

[An open source selection of design principles](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-f/) [principles.design], originally used by companies 
like Airbnb and people like Tim Berners-Lee.

[Design Lab has a free, 7-day email course on Sketch](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-z/) [trydesignlab].

 

## Logo Dancing



[10 logo design trends that&#8217;ll dominate 2018](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-a/) [thenextweb].

[3 UX features 
Medium deactivated, the design reasons behind those moves](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-f/) [uxplanet], (and why those may have been bad decisions).

[6 interesting and actually kind of plausible design predictions for 2018](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-z/) [webdesignerdepot].

 


 

## Home Run



[Free, high-resolution (4K) iPhone X mockups](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-m/) [freeui.design].

[Your site design 
should include homepage links](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-c/) [nngroup].

 

 

## The Tables Are Turned



[Why all designers should read Cyberpunk](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-x/) [magenta]. &#8220;It&#8217;s awesome,&#8221; would be my argument.

[InVision has created a US $5 million fund aimed at design startups](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-m/) [invisionapp].

[How to go about designing tables for reusability](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-c/) [uxdesign].

[unDraw](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-q/) [blog.prototypr] is a collection of MIT-licensed illustrations for use in your projects.

 


## Burn the Midnight Oil



[Alva](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-x/) [meetalva] is an open source design tool for cross-functional product teams - you can design using the same components your engineers are using..

[Fizzl](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-m/) [fizzl] is a tool for grabbing feedback from clients, team members and customers super fast.

[Midnight](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-c/) [midnightsketch] is a dark Sketch theme for macOS.

[10 design books to help you start the year off on a good foot](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-q/) [fastcodesign].

 

## After After Effects



[Some video game user experience awards](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-f/) [blog.prototypr]. A good reminder that UX is everywhere!

[A guide to taking your After Effects animations to CSS transitions and keyframes](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-z/) [smashingmagazine].

[How to do the &#8220;discovery&#8221; part of the design cycle on the cheap](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-v/) [alistapart].

[How to think about developing the UX for enterprise apps](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-e/) [sitepoint].



 

 