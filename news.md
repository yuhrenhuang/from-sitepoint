### *News/business*

## It All Ads Up



On February 15, [Chrome will start blocking &#8220;non-compliant&#8221; ads on all sites](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-f/) [venturebeat].

[Facebook&#8217;s Newsfeed algorithm will now penalize &#8220;engagement bait&#8221; posts](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-z/) [arstechnica]. Like if you agree this stuff has to stop!

[Facebook will now tag you if someone puts a photo of you on the social network, even if they don&#8217;t tag you](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-v/) [buzzfeed]. Facial recognition, innit.

There are already runaway, monomaniacal, superintelligent &#8220;artificial intelligences&#8221; that operate with a scorched-earth policy in pursuit of their goal. [They&#8217;re called &#8220;start-ups&#8221;](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-e/) [buzzfeed].

With the previous link a good example: yep, [tech no longer gets breathless, uniformly positive coverage](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-s/) [wired].

[WhatsApp has a month to stop sharing data with Facebook](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-g/) [bloomberg]. In other news, WhatsApp was sharing data with Facebook - a thing I thought was definitely not 
happening.

Cryptocurrency mining Android malware is [so aggressive it can physically harm phones](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-w/) [arstechnica].

 



## Canary in a Sun Mine



[Traffic to sites like Facebook, Apple and Microsoft were briefly routed through Russia](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-z/) [arstechnica], which is slightly concerning, I guess.

[Three men pleaded guilty to creating the Mirai botnet last week](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-v/) [wired]. It was a Minecraft scam, obviously.

Thursday the United States Federal Communications Commission will vote on whether to repeal net neutrality rules.  [This article has a list of some members of congress](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-e/) [motherboard.vice] and two facts about them: they signed a letter asking FCC chairman Ajit Pai to repeal net neutrality, and they also received some money from the telecom industry. Weird!

[Patreon just did an abrupt about-face over a planned change to the way the platform&#8217;s fees worked](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-s/) [blog.patreon]. There had been some backlash, they're hoping this will change to frontlash.

[Dead coal mines are becoming alive solar farms](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-g/) [qz].

Inside Uber&#8217;s self-driving program: [it&#8217;s very slow 
going](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-w/) [buzzfeed].

[Daneel](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-yd/) [daneel] is an AI built to help you with cryptocurrency, which might be the trendiest possible thing.

 

## Not Neutrality



Sooooo yeah, [the United States Federal Communications Commission scrapped Obama-era net neutrality rules](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-v/) [nytimes]. This is fine.

[Hackers hijacked multiple Starbucks locations to mine cryptocurrency with customers&#8217; laptops](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-e/) [motherboard.vice].

[Google&#8217;s summary of The Year in Search](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-s/) [trends.google], people liked some things a lot, and hate-searched a lot of other things.

[How to find a niche in the thriving WordPress ecosystem](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-g/) [sitepoint].

 ## Look Before You Leap



Great, [now it looks like North Korea are stealing Bitcoin](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-q/) [arstechnica].

It sounds like [the United States Federal Communications Commission will reclassify smartphone usage as broadband, making wired broadband availability a bit better than it is](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-a/) [motherboard.vice]. In that case, I&#8217;m going to reclassify these hard candies I found in my couch as my rent money!

[Secretive company Magic Leap has finally revealed their product](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-f/) [rollingstone]. I mean, looks good, I guess?

[Yeah, so Apple does slow down older phones due to battery degradation](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-z/) [techcrunch]. &#8220;But in a good way!&#8221; the company claims.

[Google Maps is sooooo far ahead of any other mapping option](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-v/) [justinoberirne].

 
## Welcome to the Desert of the Real



Report: [lots of companies (Amazon, Verizon, FACEBOOK ITSELF) that are advertising jobs on Facebook are excluding older workers from seeing their ads](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-a/) [propublica].

You can stop worrying about hackers, [a new type of computer named MORPHEUS is coming and will fix everything](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-f/) [motherboard.vice].

Google put a &#8220;Chrome Installer App&#8221;, that just installs Chrome, on Microsoft&#8217;s special Windows App Store, then Microsoft yanked it when they realised. [Proposal: this whole thing is stupid and childish and not appropriate behavior for multibillion dollar companies](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-z/) [arstechnica].

[Google&#8217;s going to fight Android fragmentation by forcing new features into all apps in 2018](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-v/) [arstechnica].

There was a report that Apple was planning on combining iPhone, iPad and Mac apps for a single user experience. [Some thoughts on that idea](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-e/) [shapeof].

[&#8220;How I went from programming with a feature phone to 
working for an MIT start-up&#8221;](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-s/) [medium.freecodecamp].

[Taskful](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-g/) [bigbold] looks like a nice little task manager.

[Viral Loops Pages](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-w/) [viral-loops] offers an easy way to create nice landing pages that&#8217;ll encourage referrals.

 ## Shell Out



[A UX designer didn&#8217;t learn of her friend&#8217;s death cos an algorithm decided it wasn&#8217;t news](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-f/) [twitter/hellchick]. A devastating story.

[Twitter&#8217;s started enforcing its new anti-hate-speech rules](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-z/) [arstechnica].

[Deep Empathy](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-v/) [deepempathy] is an interesting/crazy experiment: what if we could get people to care more about disasters in far-away countries by making them look like their home towns.

[CoinShilling](o.sitepoint.com/t/y-l-klaykl-tllkjyqul-e/) [coinshilling] shows you which cryptocurrencies are being discussed on social media, 
helpful for deciding which coins to buy (or sell, I guess).

[Daily](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-s/) [daily] lets you set up video calling for you or your company by just grabbing a free domain and sending people the link.

 ## Brainwave



[Bitcoin futures are now being traded](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-a/)! [bloomberg] In other news, I still don&#8217;t understand how Bitcoin or futures trading work.

[How Reddit and others broke the internet to support net neutrality](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-f/) [arstechnica]. The last time someone used the phrase "break the internet", it was that picture of Kim Kardashian, just to let you know the stakes.

[Scienticians injected information into the brains of monkeys](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-z/) [nytimes]. It was a tip about a hot new cryptocurrency, of course.

[Science stuff 
we learned in 2017](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-v/) [nytimes]. To be honest, &#8220;I&#8221; just learned this stuff today.

[Twitter has threads now!](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-e/) [blog.twitter] It&#8217;s either gonna get a lot harder to handle, or it will stay the same.

[Roadmap](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-s/) [onroadmap] is a project forecasting app for teams.

 
## Change of Heart



[1.4 billion clear text credentials were discovered in a single database](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-v/) [medium/4iqdelvedeep]. So now you know the Christmas gift to get for the budding &#8220;massive cybercriminal&#8221; in your life!

[An amazing story of how an Apple Watch saved a man&#8217;s life](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-e/) [9to5mac]. Guess I&#8217;m buying three Apple Watches for everyone I know, then.

[A look at China&#8217;s new experiment in social ranking](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-s/) [wired]. Like a credit score, but for every aspect of your life, and much more dystopian!

[This piece on 
Spotify and its effect on the music industry](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-g/) [thebaffler] is worthwhile, though if I had to pick its genre, &#8220;easy-listening&#8221; would be at the bottom of the list .

And [24 productivity tools that&#8217;ll help with pretty much everything](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-w/) [sitepoint].



## On Autopilot



[The Iranian government is blocking the internet to try to shut down anti-government protests](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-z/) [motherboard.vice]. 2018 appears to be picking up right where 2017 left off, bleak/scary news-wise.

[New research suggests some ad targeters are now pulling data from browsers&#8217; built-in password managers](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-v/) [theverge].

New year, new Apple vulnerability. You know how you can link your Mac and phone to send SMS messages? And you can also just ask Siri to send them too? [Yeah, that&#8217;s a problem](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-e/) [medium/@khaost].

[A longread on 
Estonia, the digital republic](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-s/) [newyorker].

And it turns out there *are* people who aren&#8217;t nervous about robots taking over their jobs - [the Swedes](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-g/) [nytimes].

[Y Combinator&#8217;s B2B buying guide](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-w/) [ycb2bguide].

[How to plan your ideal year](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-yd/) [medium/personal-growth]. Either read this now, or in exactly a year&#8217;s time.

 
## Speak for Yourself



Oooh&#8230;. hmmm&#8230; [A fundamental flaw in Intel chips means Linux and Windows devs are scrambling to make significant, performance-impacting updates to guard against the security bug](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-v/) [theregister]. Oh, and macOS and other similar operating systems will need an update too.

[Google appears to have produced voice-generating AI that&#8217;s indistinguishable from human speech](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-e/) [qz]. I can&#8217;t think of any way such a technology could be abused, so this is great news!

A music publishing company representing artists like the Beach Boys and - appropriately - Rage Against the Machine has [hit Spotify with a US 1.6 billion lawsuit for failing to pay proper royalties](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-s/) [gizmodo].

At the end of 2017, Facebook announced it was removing the &#8220;disputed&#8221; flags it had added to fake news in the newsfeed. Turns out they don&#8217;t work, so they&#8217;re adding &#8220;related articles&#8221; components instead. [Here&#8217;s why](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-g/) [medium/facebook-design].

Cryptojacking -where sites hijack your device to mine cryptocurrency, [is becoming a bit of a problem](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-w/) [wired].

[3things](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-yd/) [3things] has you pick your three most important things for 2018, then emails you to bug you about them every month. And here I was using crushing guilt - instead of a fancy service- like an animal.

 
## The Melting Pot



Some updates below on recently-discovered processor security flaws Spectre and Meltdown. Stay safe out there everyone!

[Amazon and Google are saying fixing the flaws won&#8217;t slow down their - and therefore much of the web&#8217;s - infrastructure too 
much](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-s/) [gizmodo].

[How to make sure the flaws don&#8217;t ruin your life](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-g/) [lifehacker]. [(Apple&#8217;s response is here, since it came out after that article was released)](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-w/) [support.apple].

In other news, [Mark Zuckerberg&#8217;s personal challenge for 2018 is to fix 
Facebook](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-yd/) [recode]. Good idea, CEO of Facebook!

[An ex-NSA hacker is building an AI to find hate symbols on Twitter](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-yh/) [motherboard.vice]. That is one AI I feel a bit sorry for.

[6 pre-made Trello boards that&#8217;ll help you get big goals done](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-yk/) [trello].

On a similar tip, [one Wired author details the self-improvement chatbots they spent a week interfacing with](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-yu/) [wired]. I signed up to Woebot, mentioned in the article, after reading it, I&#8217;ll let you know if I become a much better person!

[30 new top-level domains designers and developers may find useful](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jl/) [webdesignerdepot].

 

## Locked Down



[Everything known so far about those vulnerabilities discovered in modern CPUs](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-a/) [gizmodo].

[There&#8217;s a warehouse full of Otto&#8217;s smart locks out there, but the company has folded](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-f/) [engadget].

[There are reports that Spotify has secretly filed for its IPO](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-z/) [axios].

[Basecamp is now paying all its employees - none of whom live in San Francisco - as if they did](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-w/) [m.signalvnoise], which is just a generally amazing thing for a company to do.

[Fedo](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-yd/) [fedo] is a tool for developers, designers, and product managers to use to keep feature specs organized and not &#8220;nowhere&#8221;.

[Ideas on ways to handle client tasks like a pro](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-yh/) [sitepoint].

