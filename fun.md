### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

[Google&#8217;s new photo apps - Storyboard, Selfissimo, and Scrubbies are well-named and probably great!](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-g/) [research.googleblog].

[Black Out](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-w/) [itunes.apple] is an app for blacking sensitive info/images out of your pictures.

[SETI are going to start listening in to the &#8220;piece of galactic driftwood&#8221; Oumuamua to see whether it&#8217;s an alien warship or whatever](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-yd/) [scientificamerican]. It definitely is, we&#8217;re definitely doomed. Just one man&#8217;s opinion.


[Rainbrow](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-g/) [macstories] is a game for the iPhone X which you play by raising your eyebrows, [something I think Millhouse would be adept at](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-w/) [youtube]. The future is weird.

Consumers will soon be able to leverage poultry facial recognition (seriously) and the blockchain (of course) to [see the life of their chicken before it&#8217;s killed and they consume it](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-yd/) [qz]. Seriously, the future is really, really weird.

[Pitch Deck](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-yh/) [kickstarter] is a card game where the object is to make ridiculous pitches for very bad start-up ideas. If you&#8217;re good at this game, it&#8217;s probably a bad sign for the world.

[SitePoint is sponsoring India&#8217;s biggest hackathon in March!](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-yk/) [hackinthenorth] I&#8217;m stoked we&#8217;re involved in something like this, and I can&#8217;t wait to see what comes out of it! Here are last years projects, [I like the sound of the mood tracking app, also the VR glove](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-yu/) [hackinthenorth].

Finally, [this amazing deep dream adaptation continually zooms into art to find yet more art](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-jl/) [youtube].


[These headphones will work in extremely cold temperatures, look to be very comfortable](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-yd/) [indiegogo].

[Erase All Kittens](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-yh/) [kickstarter] is a game that&#8217;ll teach and inspire girls to code.

[The 50 best games of the year, according to Polygon](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-yk/) [polygon]. Realised I&#8217;ve played one, which is the number one game, so I guess I kinda nailed it?

Finally, [sooooo many people are still using &#8220;password&#8221; and &#8220;123456&#8221; as their 
password](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-yu/) [motherboard.vice]. Do this count lazy devs testing their infrastructure? No? Oh dear.

There you have it, a Wednesday Versioning for ya. I'll be back with more tomorrow, but we're approaching the end of the week, and therefore the end of regular Versioning editions for 2017! What a time!

Back tomorrow, but in the meantime I'll be hurriedly changing all my passwords from "pasdword" (I made hundreds of typos while aiming for t



Curated by [Adam](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-jr/).

[*Wait, why am I here, what is this?*](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-jy/)

 
### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

I can&#8217;t decide if this is the saddest or funniest Bitcoin story I&#8217;ve read. Meet the guy who threw away a fortune in Bitcoin, [so is now methodically digging around a landfill](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-yh/) [gizmodo]. If only Daneel had been 
around!

A very exciting (to me) upcoming book: [Shift Happens, about the history of the computer keyboard](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-yk/) [medium/@mrwichary].

[Indie game darling *Fez* is now on iOS!](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-yu/) [itunes.apple].

Finally, [someone 
actually got broadband to work through a wet string](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-jl/) [motherboard.vice]. So in, like, 20 years, you can tell your child who&#8217;s whining about their slow brain interface, or whatever, that &#8220;back in my day, we had to use string to get the internet, and we walked uphill through snow to get the string. Both ways!&#8221;

There's Versioning for Thursday, and what a good Thursday it was. Between Minecraft botnets, trashy Bitcoin and string-powered broadband, it's been a confusing day for the internet.

I'm sure tomorrow will be filled with more! See you then, via whatever medium you're using. Leaves? Bread? Whatever works!



Curated by [Adam](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-jy/).

[*Wait, why am I here, what is this?*](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-jj/)

 ### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

[John Gruber&#8217;s take on the amazingly overpowered and eye-wateringly-expensive iMac Pro](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-w/) [daringfireball]. They&#8217;re aimed at people who want to do more than just Spotify and link-finding, so I will save my $12,000, thank you 
very much. Also, I don&#8217;t have $12,000 to spend on anything fun.

[Resilient Web Design](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-yd/) [resilientwebdesign], to quote from the intro: &#8220;[combines] the most resilient ideas from the history of web design into an approach for building the websites of the future.&#8221;

Finally, this came way too late for me, but it&#8217;s worth reading as we get into the business-end of holiday season and the work Christmas parties begin: [have two 
drinks at a party](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-yh/) [lifehacker].

There is Versioning for another week, how did that happen? I'll have more for you next week, which will be the last week of regular Versioning programming in 2017. We have a lot planned for 2018, which I'll get into soon-ish!

But in the meantime, I just have a lot planned for my weekend. Well, one thing: I'm seeing Star Wars! I'm very excited, I only learned this morning that Benecio del Toro is in it! Also, Mark Hamill! What a development!

Back Monday, fully aware of what a porg is. I hope your weekend is as successful.



Curated by [Adam](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-yu/).

[*Wait, why am I here, what is this?*](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-jl/)

 
### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

Today in &#8220;things Adam took way too long to experience but were totally worth it when he did&#8221; - [*The Prophet*, a recent episode of podcast and treasure Reply All, is very incredible](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-e/) [gimletmedia]. It starts with an 
assault in Mexico, and goes to some truly unexpected places from there.

[This is the best thing I&#8217;ve read about *The Last Jedi* so far](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-s/) [theringer]. Major spoilers, obviously! Both to the movie, and to life in general.

[Go ahead, mine Bitcoin with a pen and paper](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-g/) [righto].

Finally, if you like any or all of: *Mad Max*, *Discworld*, steam punk in general 
and wheeled cities driving around a post-apocalyptic world (who doesn't?) - [you will be *very* into the trailer for *Mortal Engines*, a new Peter Jackson film out next year](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-w/) [youtube/universal].

That right there was the second to last regular Versioning for the year. Let me pass on a little secret for you though - *I'll be sending another little bonus Versioning soon. *

That's all I'll say right now, but I'm hoping you'll find whatever I send more interesting than listening to your family during your holidays! I've set a high bar, I know, but I think I'll manage it!

'Til then, I just bought a new notebook so will work on getting some cryptomining done by tomorrow. I think I remember how to do long-division, can't be that hard.

See ya tomorrow for one last normal Versioning for the 
year.



Curated by [Adam](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-yh/).

[*Wait, why am I here, what is this?*](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-yk/)

 ### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

I love this a lot: [a system that produces realistic looking sunlight and shadow](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-yd/) [creativeapplications]. So long, Seasonal Affective Disorder.

[What to do when Bill Gates sends you a 30LB Pusheen](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-yh/) [redditgifts]. Important life advice.

[An iced-tea company&#8217;s stock has tripled after adding the word &#8220;blockchain&#8221; to its name](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-yk/) [arstechnica]. This is fine.

Speaking of blockchain things: [Crypto Bar](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-yu/) [github/geraldoramos] is a 
macOS menubar app for tracking your fav cryptocurrencies.

In time for the holidays, computer games!

*Caper in the Castro*, the first LGBTQ computer game that was originally released in 1989, [is now on the Internet Archive for you to play](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-jl/) [motherboard.vice].

If meditation isn&#8217;t your thing, [there&#8217;s always *The Sims*, which is kinda the same thing](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-jr/) [gq].

Finally, uh, [the actual real Civilization VI just came out on the iPad](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-jy/) [artechnica].

That was the final regular Versioning for the year! It got a little long-winded towards the end there, but I just didn't want to leave you without stuff to read over the break!

I'll be properly back over the first week of the absurdly futuristic year 2018. By then we'll all be living on a Moon base and have robot butlers and actual real hoverboards.

I hope you'll still have an inbox with which to read my missives and access my links, and until you don't I'll continue sending these out to brighten your day.

Thanks for a great 2017, see ya next year!



Curated by [Adam](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-jt/).

[*Wait, why am I here, what is this?*](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-ji/)

 ### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

[Guardian Circle](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-yk/) [guardiancircle] is a new safety-focused social network that&#8217;s built to quickly mobilize people to help you when you&#8217;re in trouble.

[A guide to preventing cryptocurrency miners hijacking your browser](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-yu/) [howtogeek]. If you use Chrome, there&#8217;s an extension, for a start. But Opera [has it built-in](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-jl/) [thenextweb].

With the hype around cryptocurrency heating up by the day, a Dutch organization took it to the obvious extreme: [they worked out how much crypto could be mined using 
human body heat](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-jr/) [motherboard.vice]. If we accidentally invent *The Matrix* because of this stuff, I&#8217;m kind of fine with it, to be honest?

Finally, [add the New York Times&#8217; space calendar to your own calendar and get reminded when cool space stuff will happen in 2018](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-jy/) [nytimes]. Finally, a reason to use calendars!

As your calendar predicted - another Versioning for another Thursday. (If Versioning actually is on your calendar, you sound like a massive fan so thank you! Everyone else, carry on.)

I'll be back for Friday's edition on... Friday, I think. Until then I'll be learning jiu-jitsu, trying to run up as many walls as I can, and investing in a lot of leather and some armless sunglasses. (Seriously, how good was *The Matrix*!?)

See you then, unless you're back in Zion already - terrible WiFi there.

### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

Remember that weird star we found that was sometimes eclipsed by something? And everyone - me, for example - got excited because they thought it might be an alien structure or a swarm of structures or something awesome?! [Well, it pretty much definitely 
isn&#8217;t that](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jr/) [motherboard.vice]. Curse you, science!

[A checklist for cleaning up and preparing all your digital stuff for the year](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jy/) [smashingmagazine].

[And a comprehensive guide to making your home router secure](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jj/) [motherboard.vice].

[Watch an AI learn and pretty much master *Super Mario Bros.*](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jt/) [thenextweb] and despair for the future of the world.

[This Chrome extension gives you the real price of an Airbnb stay, accounting for fees](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-ji/) [chrome.google].

Finally, [if birds left trails in the air, this is what they would look like](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-jd/) [nationalgeographic]. 50/50 
beautiful/terrifying.

### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

[This app collects your spare change to help people make bail](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-yh/) [wired].

[themer](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-yk/) [github/mjswensen] takes a set of colors and generates themes for your code editors, terminal windows, wallpapers, all that stuff.

If you got a new Mac over the holidays, good for you! [This guide explains the process of setting up a development environment on your new toy](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-yu/) [github/sb2nov].

[A survey of how realistic *Black 
Mirror&#8217;s* tech is](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-jl/) [quartzy.qz]. Just in case you didn&#8217;t find that show terrifying enough already. (Spoilers abound in that link, by the way. Both for the show and for life.)

Finally, the latest company to use a tenuous blockchain tie-in to juice its stock? [The parent company of Hooters](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-jr/) [arstechnica].

### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

If you used your holiday to jump into the blockchain/cryptocurrency world, [Blockfolio](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-yh/) [blockfolio] is a mobile app to manage your portfolio.

[Apple&#8217;s open sourcing the code for the Lisa](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-yk/) [bbc].

[An ingenious way to hack iOS trivia game HQ](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-yu/) [hackernoon] using machine vision and a bit of Python.

Finally, [Reroute Twitter](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-jl/) [github/schukin] is an iOS app (that you can set up) that looks like Twitter, but opens a Kindle book instead. If your feed is too bleak and 
depressing this will help, unless you&#8217;re reading Sylvia Plath, or *The Diving Bell and the Butterfly*, or whatever.


### *Everything else (apps, fun tools, gaming, culture, funny stuff)*

[Quantum Game](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-yd/) [github/stared] is an in-browser puzzle game that uses quantum mechanics.

[*An Incomplete Timeline of What We Tried*](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-yh/) [motherboard.vice] is a **devastating** experimental short story that&#8217;s like a cross between Margaret Atwood fiction and some Thom Yorke lyrics.

[Astronauts on the International Space Station got *The Last Jedi* beamed up to them](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-yk/) [arstechnica]. I would&#8217;ve assumed they&#8217;d put such vital information into the memory systems of an R2 unit, but I&#8217;m old-fashioned like that.

And finally, and speaking of which, I went and saw 
the *Star War* over the weekend, so naturally since then I&#8217;ve been either avoiding or gorging on reviews and hot takes. [This one was interesting, came at it from a different angle](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-yu/) [motherboard.vice].

 