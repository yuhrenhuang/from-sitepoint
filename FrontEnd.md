


[![JAMF Now](http://i1.createsend1.com/ei/y/F9/970/784/222956/csfinal/Frame27.png)](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-y/)

### *Front-end*

## &#8203;Simple Apple Device Management | Jamf Now

Jamf Now is a device management solution for your iPad, iPhone and Mac devices at work.

Jamf Now makes management tasks like deploying Wi-Fi passwords, distributing apps and enforcing passcodes, simple; no IT required.&#8203;

[Create your free account today!&#8203;&#8203;](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-j/)


## You Can Count On It



First up, [this single line of code will make your HTML responsive](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-t/) [medium.freecodecamp].

[How to build a simple blog using React and GraphQL](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-i/) [sitepoint].

[React Starter Kit](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-d/) [glitch] is a little course to help you get your head around React, made using Glitch, which is kinda cool

[Introducing ABEM](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-h/) [css-tricks], a nice little adaptation of BEM.

[A guide to using CSS counters](http://go.sitepoint.com/t/y-l-klltiid-tllkjyqul-k/) [pineco].

 


[![SaneBox](http://i1.createsend1.com/ei/y/03/879/890/211708/csfinal/Frame282.png)](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-y/)

## &#8203;Clean up your inbox today (& keep it that way forever) with SaneBox

SaneBox brings sanity back to your inbox by prioritizing what's important, removing spam & junk, grouping newsletters together, and automating tedious tasks.

[Sign up today and get an exclusive $25 credit as a new user!](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-j/)


## Focus In



First up, [an overview of tools to check a site&#8217;s accessibility](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-t/) [css-tricks].

[6 
ways to clear cache in the browser](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-i/) [calendar.perplanet], handy if you accidentally ship a bad release of a front-end asset with a long cache lifetime.

[Ways to keep a parent element visible while child :focus](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-d/) [css-tricks].

Oh, and an &#128680; actual good deal alert &#128680; here for you: [$29 for access to all the SitePoint books and courses you need to solidify your status as a front-end genius](http://go.sitepoint.com/t/y-l-klltiht-tllkjyqul-h/) [sitepoint], including good 
coverage of CSS, ES6, React, Angular 2, Bootstrap 4, responsive web design and more! There&#8217;s a lot there and you&#8217;ll save $435, a large and oddly-specific amount. Deal ends today!

 


## Make Billing Easy with FreshBooks

FreshBooks is an easy-to-use accounting software designed to make running your small business easy, fast and secure. Spend less time on accounting and more time doing the work you love. 

[Try FreshBooks now for *free*](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-y/).


## Easy E



First up, [5 old-fashioned things you no longer need to do when working with CSS](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-j/) [ryanogles].

Some [CSS Grid resources and tools for ya](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-t/) [cssgrid].

[Ease](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-i/) [github/roberthein] looks like a nice little animation library.

[x0](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-d/) [github/c8r] is a zero-config React dev environment and static-site generator.

And here&#8217;s a tool for [examining the performance of React components](http://go.sitepoint.com/t/y-l-kljllll-tllkjyqul-h/) [github/nitin42].

 



 

[![Developer Economics Survey](http://i1.createsend1.com/ei/y/F9/043/32B/182520/csfinal/Frame331.png)](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-y/)

## What&#8217;s hot in development in 2018?

Calling all developers to raise their voices and speak their minds about the future of the software development industry! 

[Take the survey and voice your thoughts.](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-j/)

There's some *major *prizes up for grabs, including **a Pixel 2 phone,** **an iPhone 
X,** **Raspberry Pi 3 kits **and a **Nintendo Switch.**

It's likely to be the best developer survey you've ever taken (srs). 

 



## Heart of Gold



[Material Design components for Angular](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-y/) [material.angular].

[A guide to building a nice 
little memory matching game in JavaScript](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-j/) [scotch].

In December, Smashing Magazine ran a test for readers to show off their front-end performance chops. [Here are the results and the winners](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-t/) [smashingmagazine].

[The 100 most hearted Pens on CodePen in 2017](http://go.sitepoint.com/t/y-l-klilkjk-tllkjyqul-i/) [codepen].

 


## Seed Money



First up, if you haven&#8217;t dived in yet, [a guide to learning CSS Grid](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-y/) [learncssgrid].

[QSS](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-j/) [tomhodgins.github] is a simple query syntax for handling element queries in CSS.

[Shards](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-t/) [github/designrevision] is a modern, free Bootstrap 4 UI toolkit.

[react-seed](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-i/) [github/uruit] is a pretty literally-named React seed project.

While we&#8217;re talking starter projects, [ReactPWA](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-d/) [reactpwa] is an upgradeable boilerplate for PWAs. Server-side rendering, speedy pages, optimized UX - all that stuff.

React v Vue, Reason, Storybook: [just some of the top JavaScript trends in 2018](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-h/) [x-team].

And React and Vue may be mortal enemies, but [React-Vue](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-k/) [github/smallcomfort] lets you bring them together to make something great.

Smashing Magazine seems to be pretty focused on performance right now! [Here&#8217;s Vitaly Friedman&#8217;s front-end performance checklist](http://go.sitepoint.com/t/y-l-klijtit-tllkjyqul-u/) [smashingmagazine].

 


## Add It Up



First up, [a round-up of all Codrops resources in 2017](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-y/) [tympanus].

[Position: sticky can 
handle your sticky navbars for you, no JavaScript needed](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-j/) [advent2017.digitpaint].

[Immer](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-t/) [github/mweststrate] is a tiny package that lets you work with immutable state in an easier way.

[How to document better](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-i/) [ybrikman].

[How 
and why to write higher order components](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-d/) [paulgray].

An intro to an upcoming browser feature: [additive animation with the web animations API](http://go.sitepoint.com/t/y-l-kltkvy-tllkjyqul-h/) [css-tricks].

 


 

[![Bluehost](http://i1.createsend1.com/ei/y/4B/CC5/B55/222838/csfinal/Frame351.png)](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-y/)

 

## Get a Free Year of SitePoint Premium thanks to Bluehost

&#8203;Get 60% off web hosting from Bluehost plus a free domain, and we'll give you a free year of SitePoint Premium.

[Get started now!](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-j/)

 

## Await On Me



First up, [Wes Bos explains async/await](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-t/) [youtube/dotconferences].

Indefatigable SitePoint alum Louis Lazaris shares [his favorite front-end tool finds of 2017](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-i/) [css-tricks].

[A guide to understanding the range input, and making your sliders more consistently slidey](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-d/) [css-tricks].

[The complete set of Instagram image filters in pure CSS](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-h/) [github/picturepan2].

And [5 design fears that can be put to one side thanks to CSS Grid](http://go.sitepoint.com/t/y-l-kltdzt-tllkjyqul-k/) [css-tricks].

 


[![Pendo](http://i1.createsend1.com/ei/y/62/F86/450/224527/csfinal/Frame291.png)](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-y/)

## &#8203;Deliver Exceptional Customer Experiences in Your Product

&#8203;Pendo delivers the only complete platform for Product Teams. Understand product usage, collect user feedback, measure NPS, assist users in their apps and promote new features in product.

[Get a Pendo demo today.](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-j/)


## Post-haste



First up, [JavaScript Code to Go offers useful JS snippets for common use-cases](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-t/) [codetogo].

[Boardgame.io](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-i/) [google.github] offers state management and a few other handy things for turn-based board games. Might be quite transferable to non-boardgame things, too.

[vue-inspector](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-d/) [github/calirojas] is a Vue.js inspector for mobile devices.

[A look at creating manageable utility systems with CSS variables](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-h/) [seesparkbox].

What&#8217;s better? [novel or tried-and-true image formats](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-k/) [css-tricks].

And an &#128680; **actual good deal alert &#128680;**, this time for a [50% saving on Todd Motto&#8217;s Ultimate Angular courses](http://go.sitepoint.com/t/y-l-klaykl-tllkjyqul-u/) [ultimateangular]. Use coupon code **&#8216;SITEPOINT_SPECIAL&#8217;** at checkout, that&#8217;s the best deal you&#8217;re gonna get unless you&#8217;re good friends with Todd and can get him to teach you in exchange for beer or 
a dinner or something. In which case, you sound great, well done!



[![pCloud](http://i1.createsend1.com/ei/y/F0/CA3/421/230526/csfinal/VersioningBanners1.png)](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-y/)

## &#8203;Good News! We've Partnered with pCloud

"So what?" we hear you say. Well this is what.

We're offering YOU, our Versioning legends, this exclusive offer... 

[*Get 2 TB of storage for 3 months for just $9.99* *with pCloud.*](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-j/)

pCloud are a secure cloud storage 
solution that allows you to upload and access your files on any device, without taking up precious storage space. 

[Try it out now!](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-t/)

 

 

## Post-haste



First up, is jQuery still relevant? According to Remy Sharp: [yep](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-i/) [remysharp].

[A guide 
to implementing Material Design Motion with React](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-d/) [codeburst].

[Tips on testing React components](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-h/) [medium/@skidding].

[PostHTML](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-k/) [github/posthtml] is a tool for transforming HTML using JavaScript.

We also have an &#128680; **actual good deal alert **&#128680; here for you: [save $415 on our beginner full-stack dev collection: books on CSS, JavaScript, PHP+MySQL, Rails, all that good stuff!](http://go.sitepoint.com/t/y-l-klmltl-tllkjyqul-u/) [sitepoint].


## Origin of the Spec-ies



[A journey into the world of pure CSS images](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-t/) [blog.prototypr].

[11 things 
learned reading the CSS Grid specification](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-i/) [medium.freecodecamp].

[Object-Oriented CSS and CSS Grid, together at last](http://go.sitepoint.com/t/y-l-kljdjhy-tllkjyqul-d/) [keithjgrant].

 


[![Logaster](http://i1.createsend1.com/ei/y/D1/55F/51F/180020/csfinal/VersioningBanners.png)](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-y/)

## Want to create your logo in a minute?

Logaster is a quick and easy online tool that lets you generate different elements of your corporate identity, including logo, business cards, letterheads, favicons and envelopes. Before you can create a favicon or business card, you will first need a logo, but don&#8217;t worry, it&#8217;s very easy. The process is simple, you choose the icon, font, elements positioning and colors. After the logo is created, the service will generate a great number of beautiful and ready to use business cards, favicons, letterheads and envelopes based on your logo. You 
can then choose and save your design.

Logaster is an online service for creating logos and corporate identity elements. There is no need to contact a designer and describe what logo you want. Just spend few minutes and create it yourself!

[Try it today](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-j/) and get 15% off using the code: **LOG15SP**


## Out of the Box



First up, [how React and Vue measure up against each other](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-t/) [javascriptreport].

[PixiJS](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-i/) [pixijs] is a web rendering library that&#8217;s basically an easier alternative to WebGL.

[A guide to getting stuck in to CSS Custom Properties](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-d/) [24ways].

[An intro to CSS Flexible Box Layout](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-h/) [developer.mozilla].

[How to use SVG to create a duotone image effect](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-k/) [css-tricks].

[A very thorough recap of everything that happened in front-end in 2017](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-u/) [blog.logrocket]. Thanks to BrianNeville-O&#8217;Neill on the Twitters for sending that through!

A new SitePoint book for ya to feast your eyes and brain on: [Front-end Performance](http://go.sitepoint.com/t/y-l-klydjul-tllkjyqul-o/) [sitepoint].

 


[![Be Theme](http://i1.createsend1.com/ei/y/9B/24D/9A1/191356/csfinal/be-theme-versioning.jpg)](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-y/)

## 290+ Pre-built Websites, Just a Click Away

Use Be Theme&#8217;s 290+ pre-built websites. They&#8217;ll help you find a perfect fit for your client. 

Be Theme&#8217;s collection of 290+ pre-built websites is by far the largest you&#8217;ll find anywhere. It covers 30+ different business niches. This makes it a perfect theme for virtually any client.

[Check out how Be Theme can help you keep your clients happy](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-j/).

 

## Surv&#8217;s Up



First up, [the results to a survey of the JavaScript world in 2017 are in, enjoy them!](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-t/) [stateofjs] There are a lot of them!

[A guide to CSS Layout and block formatting context](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-i/) [smashingmagazine].

[An intro to modern asynchronous CSS loading](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-d/) [filamentgroup].

[Design systems of CSS Grid](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-h/) [24ways].

[Font Awesome 5 is out!](http://go.sitepoint.com/t/y-l-klbyhd-tllkjyqul-k/) [fontawesome] Thanks Mateus on Twitter for alerting me to that.

 